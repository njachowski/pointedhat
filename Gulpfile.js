var gulp   = require("gulp");
var gulpgo = require("gulp-go");

var go;

gulp.task("go-run", function() {
  go = gulpgo.run("main.go", ["--arg1", "value1"], {cwd: __dirname, stdio: 'inherit'});
});

gulp.task("devs", ["go-run"], function() {
  gulp.watch([__dirname+"/**/*.go"]).on("change", function() {
    go.restart();
  });
});

gulp.task('default', function() {
  console.log("hello!")
  gulp.start('go-run');
  gulp.watch('./js/*.js', ['go-run']);
  gulp.watch('./*.html', ['go-run']);
});

