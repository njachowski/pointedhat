//var $ = require('jquery');
//var dropzone = require('dropzone');

var onDragEnter = function (event) {
    //event.preventDefault();
    $(".br_dropzone").addClass("dragover");
},

onDragOver = function (event) {
    event.preventDefault();
    if (!$(".br_dropzone").hasClass("dragover"))
        $(".br_dropzone").addClass("dragover");
},

onDragLeave = function (event) {
    event.preventDefault();
    $(".br_dropzone").removeClass("dragover");
},

onDrop = function (event) {
    //event.preventDefault();
    $(".br_dropzone").removeClass("dragover");
    $(".br_dropzone").addClass("dragdrop");
    console.log(event.originalEvent.dataTransfer.files);
};

Dropzone.options.myAwesomeDropzone = {
    dictDefaultMessage: "",
    paramName: "file", // The name that will be used to transfer the file
    url: "/upload",
    //previewsContainer: ".br_dropzone",
    maxFilesize: 2, // MB
    maxThumbnailFilesize: 5,
    accept: function(file, done) {
      console.log(file.name);
      done();
    }
};

$(".br_dropzone")
.on("dragenter", onDragEnter)
.on("dragover", onDragOver)
.on("dragleave", onDragLeave)
.on("drop", onDrop);
