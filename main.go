package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	port := ":3333"
	router := mux.NewRouter()
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./")))
	http.Handle("/", router)

	log.Println("Listening at http://localhost" + port + "\n")
	http.ListenAndServe(port, nil)
}
